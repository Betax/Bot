<?php

class Conexion{

    public static function conectar(){
        try {
            $cn = new PDO("mysql:host=localhost;dbname=loginpdo", "root", "");
            return $cn;
        } catch (PDOException $ex) {
            die($ex->getMessage());
        }
    }
}
