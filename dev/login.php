<?php
	session_start();
	include('inc/connection.php');
	include('getusers.php');
?>
	<?php
	    if(empty($_SESSION['usuario_nombre'])) { // comprobamos que las variables de sesión estén vacías
	?>
      <div class="row center">
    <form action="comprobar.php" method="post" class="col s12 center">
      <div class="row center">
        <div class="input-field col s6 center">
          <i class="material-icons prefix white-text center">perm_identity</i>
          <input id="icon_perm_identity" class="validate white-text center" type="text" name="usuario_nombre">
          <label for="icon_perm_identity">Usuario</label>
        </div>
				</div>
				<div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix white-text">vpn_key</i>
          <input id="icon_vpn_key" class="validate white-text" type="password" name="usuario_clave">
          <label for="icon_vpn_key">Password</label>
        </div>

      </div>
      <input class="btn blue lighten-1" type="submit" name="enviar" value="Ingresar"/>
    </form>
  </div>
	<?php
	    }else {
	?>
	        <p>Hola <strong><?=$_SESSION['usuario_nombre']?></strong> | <a h7ref="logout.php">Salir</a></p>
	<?php
	    }
	?>
