<?php
    session_start();
    include('/util/acceso_db.php');
    if(isset($_SESSION['usuario_nombre'])) {
        session_destroy();
        header("Location: index.php");
    }else {
        echo "Operación incorrecta.";
    }
?>
