<?php
	include('inc/connection.php');
	include('getusers.php');
?>

	 <?php
	    if(isset($_POST['enviar'])) { // comprobamos que se han enviado los datos desde el formulario
	        // creamos una función que nos parmita validar el email
	        function valida_email($correo) {
	            if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
	            else return false;
	        }
	        $sin_espacios = count_chars($_POST['usuario_nombre'], 1);
	        if(!empty($sin_espacios[32])) { // comprobamos que el campo usuario_nombre no tenga espacios en blanco
	            echo "El campo <em>Usuario</em> no debe contener espacios en blanco. <a href='javascript:history.back();'>Reintentar</a>";
	        }elseif(empty($_POST['usuario_nombre'])) { // comprobamos que el campo usuario_nombre no esté vacío
	            echo "No haz ingresado tu usuario. <a href='javascript:history.back();'>Reintentar</a>";
	        }elseif(empty($_POST['usuario_clave'])) { // comprobamos que el campo usuario_clave no esté vacío
	            echo "No haz ingresado contraseña. <a href='javascript:history.back();'>Reintentar</a>";
	        }elseif($_POST['usuario_clave'] != $_POST['usuario_clave_conf']) { // comprobamos que las contraseñas ingresadas coincidan
	            echo "Las contraseñas ingresadas no coinciden. <a href='javascript:history.back();'>Reintentar</a>";
	        }elseif(!valida_email($_POST['usuario_email'])) { // validamos que el email ingresado sea correcto
	            echo "El email ingresado no es válido. <a href='javascript:history.back();'>Reintentar</a>";
	        }elseif(mysql_num_rows(mysql_query("SELECT * FROM usuarios WHERE usuario_email='".mysql_real_escape_string($_POST['usuario_email'])."'")) == 1) {
                 echo "El email ingresado ya se encuentra en uso. <a href='javascript:history.back();'>Reintentar</a>";
            }elseif(mysql_num_rows(mysql_query("SELECT * FROM usuarios WHERE usuario_nombre='".mysql_real_escape_string($_POST['usuario_nombre'])."'")) == 1) {
                 echo "El nombre de usuario ingresado ya se encuentra en uso. <a href='javascript:history.back();'>Reintentar</a>";
            }else {
	            // "limpiamos" los campos del formulario de posibles códigos maliciosos
	            $usuario_nombre = mysql_real_escape_string($_POST['usuario_nombre']);
	            $usuario_clave = mysql_real_escape_string($_POST['usuario_clave']);
	            $usuario_email = mysql_real_escape_string($_POST['usuario_email']);
              $ip = $_SERVER['REMOTE_ADDR'];
	            // comprobamos que el usuario ingresado no haya sido registrado antes
	            $sql = mysql_query("SELECT usuario_nombre FROM usuarios WHERE usuario_nombre='".$usuario_nombre."'");
	            if(mysql_num_rows($sql) > 0) {
	                echo "Usuario ya registrado. <a href='javascript:history.back();'>Reintentar</a>";
	            }else {
	                $usuario_clave = md5($usuario_clave); // encriptamos la contraseña ingresada con md5
	                // ingresamos los datos a la BD
	                $reg = mysql_query("INSERT INTO usuarios (usuario_nombre, usuario_clave, usuario_email, usuario_freg, usuario_expired, membership, ip)
									 VALUES ('".$usuario_nombre."', '".$usuario_clave."', '".$usuario_email."', NOW(), TIMESTAMPADD(DAY, 1, NOW()), '1', '".$ip."')");
	                if($reg) {
	                    echo '<h5>Datos ingresados correctamente.</h5>';
	                }else {
	                    echo '<h5>ha ocurrido un error y no se registraron los datos.';
	                }
	            }
	        }
	    }else {
	?>
		<form class="col s12" action="<?=$_SERVER['PHP_SELF']?>" method="post">
      <div class="row">
        <div class="input-field col s5 white-text center">
          <i class="material-icons prefix white-text center">account_circle</i>
          <input id="icon_prefix" type="text" name="usuario_nombre" maxlength="15" class="validate white-text center">
          <label for="icon_prefix">Usuario</label>
        </div>
        <div class="input-field col s5 white-text center">
          <i class="material-icons prefix white-text center">vpn_key</i>
          <input id="icon_vpn_key" type="password" name="usuario_clave" maxlength="15" class="validate white-text center">
          <label for="icon_vpn_key">Contraseña</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s5 white-text center">
          <i class="material-icons prefix white-text center">email</i>
          <input id="icon_email" type="text" name="usuario_email" maxlength="50" class="validate white-text center">
          <label for="icon_emailx" class="white-text center">Email</label>
        </div>
        <div class="input-field col s5 white-text center">
          <i class="material-icons prefix white-text center">vpn_key</i>
          <input id="icon_vpn_key" type="password" name="usuario_clave_conf" maxlength="15" class="validate white-text center">
          <label for="icon_vpn_key">Confirmar Contraseña:</label>
        </div>
      </div>
	  <input class="btn blue lighten-1" type="submit" name="enviar" value="Registrar" />
	  <input class="btn blue lighten-1" type="reset" value="Borrar" />
    </form>
	<?php
	    }
	?>
