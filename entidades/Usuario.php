<?php

class Usuario {

    private $id;
    private $nombre;
    private $usuario;
    private $email;
    private $password;
    private $privilegio;
    private $fecha_registro;
    private $expired;
    private $membership;
    private $lastlogin;
    private $bot_time;
    private $banned;
    private $ip;

    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getNombre(){
		return $this->nombre;
	}

	public function setNombre($nombre){
		$this->nombre = $nombre;
	}

	public function getUsuario(){
		return $this->usuario;
	}

	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}

	public function getPrivilegio(){
		return $this->privilegio;
	}

	public function setPrivilegio($privilegio){
		$this->privilegio = $privilegio;
	}

	public function getFecha_registro(){
		return $this->fecha_registro;
	}

	public function setFecha_registro($fecha_registro){
		$this->fecha_registro = $fecha_registro;
	}

	public function getExpired(){
		return $this->expired;
	}

	public function setExpired($expired){
		$this->expired = $expired;
	}

	public function getMembership(){
		return $this->membership;
	}

	public function setMembership($membership){
		$this->membership = $membership;
	}

	public function getLastlogin(){
		return $this->lastlogin;
	}

	public function setLastlogin($lastlogin){
		$this->lastlogin = $lastlogin;
	}

	public function getBot_time(){
		return $this->bot_time;
	}

	public function setBot_time($bot_time){
		$this->bot_time = $bot_time;
	}

	public function getBanned(){
		return $this->banned;
	}

	public function setBanned($banned){
		$this->banned = $banned;
	}

	public function getIp(){
		return $this->ip;
	}

	public function setIp($ip){
		$this->ip = $ip;
	}
}
