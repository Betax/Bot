<?php include 'partials/head.php';?>
<?php
	    if(empty($_SESSION['usuario'])) {
	?>
	<div class="container ">
		<div class="row">
		  <div class="col s12 m12">
			<div class="card-panel white">
				<!--logo-->
			  <div class="row center">
					<img src="assets/images/user.png" class="responsive-img">
					<a class="btn-floating btn-large waves-effect waves-light teal right" href="registro.php"><i class="material-icons">add</i></a>
			  </div>
			  <!--formulario-->
			  <div class="row">
				<div class="col s12 m6 offset-m3">
				  <div class="card">
					<div class="card-content">
					  <span class="card-title black-text">Sign In ArrowBot</span>
					  <form id="loginForm" action="validarCode.php" method="POST" role="form">
							<div class="row">
							  <div class="input-field col s12">
								<input placeholder="UserName" id="firstname" type="text" class="validate" name="txtUsuario" autofocus required>
								<label for="firstname" class="active">User</label>
							  </div>
							</div>
							<div class="row">
							  <div class="input-field col s12">
								<input placeholder="Password" id="lastname" type="password" class="validate" name="txtPassword" required>
								<label for="lastname" class="active">Password</label>
							  </div>
							</div>
							<div class="card-action center">
							  <button type="submit" class="btn btn-success">Ingresar</button>
							</div>
					  </form>
					</div>
				  </div>
				</div>
			</div>
		  </div>
		</div>
	</div>
	<?php
}else{
	header ("Location: usuario.php")
	?>
	<?php
		}
?>
<?php include 'partials/footer.php';?>
