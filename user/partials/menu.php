<nav class="teal">
  <ul id="slide-out" class="side-nav teal">
    <?php if ($_SESSION["usuario"]["privilegio"] == 1) {?>
        <li><a href="cerrar-sesion.php" class="white-text"><i class="material-icons left white-text">highlight_off</i>Salir</a></li>
    <?php } else {?>
      <li><a href="https://dragonbound.net" target="_blank" class="white-text"><i class="material-icons left white-text">bug_report</i>Aplicar DragonBound</a></li>
      <li><a href="http://igunfire.com" target="_blank" class="white-text"><i class="material-icons left white-text">bug_report</i>Aplicar Igunfire</a></li>
      <li><a href="help.html" class="white-text"><i class="material-icons left white-text">help</i>Ayuda</a></li>
      <li><a href="cerrar-sesion.php" class="white-text"><i class="material-icons left white-text">highlight_off</i>Salir</a></li>
<?php }?>
  </ul>
  <a href="#" data-activates="slide-out" class="button-collapse show-on-large"><i class="material-icons">menu</i></a>
</nav>
