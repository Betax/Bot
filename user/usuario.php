<?php include 'partials/head.php';?>
<?php
if (isset($_SESSION["usuario"])) {
    if ($_SESSION["usuario"]["privilegio"] == 1) {
        header("location:admin.php");
    }
} else {
    header("location:index.php");
}
?>
<?php include 'partials/menu.php';?>
<!-- Page Layout here -->
<div class="row">

  <div class="col s12 m4 l3"> <!-- Parte izquierda -->
	<div class="row center">
		<img src="assets/images/user.png" class="responsive-img">
    </div>
	<div>
	  <ul class="collection">
		<li class="collection-item avatar">
		  <img src="assets/images/user.png" alt="" class="circle">
		  <span class="title">User</span>
		  <p><?php echo $_SESSION["usuario"]["nombre"]; ?>
		  </p>
		  <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
		</li>
		<li class="collection-item avatar">
		  <i class="material-icons circle">folder</i>
		  <span class="title">Esatdo</span>
		  <p><?php echo $_SESSION["usuario"]["privilegio"] == 1 ? 'Admin' : 'usuario'; ?>
		  </p>
		  <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
		</li>
		<li class="collection-item avatar">
		  <i class="material-icons circle green">insert_chart</i>
		  <span class="title">Title</span>
		  <p>First Line <br>
			 Second Line
		  </p>
		  <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
		</li>
		<li class="collection-item avatar">
		  <i class="material-icons circle red">play_arrow</i>
		  <span class="title">Title</span>
		  <p>First Line <br>
			 Second Line
		  </p>
		  <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
		</li>
		</ul>
	</div>

  </div>
  <div class="col s12 m8 l9"> <!-- Parte derercha -->

	 <div class="responsive-video">
        <iframe width="854" height="480" src="https://www.youtube.com/embed/xRHYbdTTpKg" frameborder="0" allowfullscreen></iframe>
     </div>
	 <div class="row">
		<div class="row">
			<div class="col s12">
			  <ul class="tabs" class="teal-text">
				<li class="tab col s3"><a href="#test1" class="teal-text">DragonBound</a></li>
				<li class="tab col s3"><a href="#test2" class="teal-text">Igunfire</a></li>
				<li class="tab col s3" class="teal-text"><a href="#test3" class="teal-text">Facebook</a></li>
				<li class="tab col s3"><a href="#test4" class="teal-text">Youtube</a></li>
			  </ul>
			</div>
			<div id="test1" class="col s12 center">
				<br>
				<a class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>DragonBound</a>
			</div>
			<div id="test2" class="col s12 center">
				<br>
				<a class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>igunfire</a>
			</div>
			<div id="test3" class="col s12 center">
				<br>
				<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FArrowBot&width=176&layout=button&action=like&size=large&show_faces=true&share=true&height=65&appId" width="176" height="65" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
			</div>
			<div id="test4" class="col s12 center">Test 4</div>
		</div>
	 </div>
  </div>
</div>
<?php include 'partials/footer.php';?>
